@include('header')
<main>
    <section class="wrapper">
        <form action="/recipes" method="GET">
            <label>
                Введите коды ингредиентов:
                <input id="ingInput" type="text" class="form-control" name="recipe" data-allowed-chars="{{ json_encode($allowedChars) }}">
            </label>
            <button type="submit" class="btn btn-primary">Сформировать</button>
        </form>
        <div style="margin-top:30px">
            <p>Подсказки по кодам:</p>
            <ul class="list-group list-group">
                @foreach ($ingredientTypes as $ingredientType)
                    <li class="list-group-item">{{$ingredientType->code}} - {{$ingredientType->title}}</li>
                @endforeach
            </ul>
        </div>
    </section>
</main>


<script>
  const inputElement = document.getElementById('ingInput');
  const allowedChars = JSON.parse(inputElement.dataset.allowedChars);

  inputElement.addEventListener('input', function(event) {
    const inputValue = event.target.value;
    const lastInputChar = inputValue.charAt(inputValue.length - 1);

    if (!allowedChars.includes(lastInputChar)) {
      event.target.value = inputValue.slice(0, -1); // Удаляем неподходящий символ
    }
  });
</script>
@include('footer')
