@include('header')
<main>
    <section class="wrapper">
        <b>Всего рецептов: {{$combinationsCount}}</b>
        @foreach($combinationsArray as $combination)
            <div style="margin: 30px 0">
                @foreach($combination['products'] as $product)
                        <p style="color: #0E51A7; font-weight: bold">type: <b style="color: #FF9E00;">{{ $product['type'] }}</b>, value: <b <b style="color: #FF9E00;">{{ $product['value'] }}</b></p>
                @endforeach
                <b>price: {{$combination['price']}}</b>
            </div>
            <hr>
        @endforeach
        <p>
            {{$combinationsJson}}
        </p>
    </section>
</main>
@include('footer')
