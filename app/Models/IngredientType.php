<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IngredientType extends Model
{
    use HasFactory;
    protected $table = 'ingredient_type';
    
    public function ingredients()
    {
        return $this->hasMany(Ingredient::class, 'type_id');
    }
}
