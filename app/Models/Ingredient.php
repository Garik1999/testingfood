<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{
    use HasFactory;
    protected $table = 'ingredient';

    public function type()
    {
        return $this->belongsTo(IngredientType::class, 'type_id');
    }
    
}
