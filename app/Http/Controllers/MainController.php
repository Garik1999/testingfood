<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\IngredientType;

class MainController extends Controller
{
    public function index() {
        $ingredientTypes = IngredientType::all();
        $allowedChars = [];

        foreach($ingredientTypes as $ingredientType) {
            $allowedChars[] = $ingredientType->code;
        }

        return  view('main',['ingredientTypes' => $ingredientTypes, 'allowedChars' => $allowedChars]);
    }

}