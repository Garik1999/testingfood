<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\IngredientType;

class RecipesController extends Controller
{
    public function index() {
        $recipeCode = str_split(request('recipe'));
        $ingredientTypes = IngredientType::all();

        $ingredients = [];
        $ingredientTitles = [];

        foreach($ingredientTypes as $ingredientType) {
            $ingredients[$ingredientType->code] = $ingredientType->ingredients;
            $ingredientTitles[$ingredientType->code] = $ingredientType->title;
        }

        $combinations = [];
        $this->generateCombinations($ingredients, $ingredientTitles, $recipeCode, $combinations);
        
        // Преобразуем результат в JSON-массив
        $json_result = json_encode($combinations, JSON_UNESCAPED_UNICODE);

        return  view('recipe',['combinationsJson' => $json_result, 'combinationsArray' => $combinations, 'combinationsCount' => count($combinations)]);
    }

    // Функция для генерации комбинаций по шаблону
    protected function generateCombinations($ingredients, $ingredientTitles, $recipeCode, &$combinations, $combination = [], $index = 0) {
        if ($index == count($recipeCode)) {
            $combinations[] = $combination;
            return;
        }
        $current_type = $recipeCode[$index];
        if (isset($ingredients[$current_type])) {
            foreach ($ingredients[$current_type] as $product) {
                $newCombination = $combination;

                $price = 0;
                if ($index != 0) {
                    $price = $newCombination['price'];
                }

                $newCombination['products'][] = [
                    'type' => $ingredientTitles[$current_type],
                    'value' => $product->title
                ];
                $newCombination['price'] = $price + $product->price;
                // $price = isset($newCombination['price'])
                // $newCombination['price'] = $newCombination['price'] + $product->price;
                $this->generateCombinations($ingredients, $ingredientTitles, $recipeCode, $combinations, $newCombination, $index + 1);
            }
        } else {
            $this->generateCombinations($ingredients, $ingredientTitles, $recipeCode, $combinations, $combination, $index + 1);
        }
    }

}